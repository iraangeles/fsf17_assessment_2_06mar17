Full Stack Foundation Second Assessment
HALFWAY THRU FSF
You have just completed half of your 40 days journey, the time has come to once again test
out your new skills!
TOPIC:
Create a simple 2-page Grocery Management module
CONCEPTS TESTED:
1. Basics of MySQL
○ Relationships between Entities
○ Query and Manipulating Database
2. AngularJS
○ Factories / Services
○ $http.post vs $http.get
3. Express
○ Routes
○ RESTful API: Handling HTTP requests
○ Connecting to a Database
RESOURCES:
You are provided with a sample groceries schema here: https://goo.gl/14f0Lg
How to generate barcodes:
http://www.barcodes4.me/barcode/c128a/{upc12}.png?IsTextDrawn=1
Example:
http://www.barcodes4.me/barcode/c128a/035200264013.png?IsTextDrawn=1
CORE REQUIREMENTS:
Page 1
1. Search form (mandatory fields)
a. Brand
b. Product Name
2. List of Groceries
a. Display a list of 20 products by default, with the Product Name arranged in
alphabetical order
b. Options to sort grocery list by Brand or Product Name from A-Z and Z-A
c. Each product in the grocery list should display the following:
i. UPC12 Barcode
ii. Brand
iii. Product Name
iv. Edit button (that will lead to Page 2)
d. Search result should include all possibilities based on keywords found in
Brand or Product Name fields
e. You may use GET or POST methods for the search
1
Full Stack Foundation Second Assessment
Page 2
1. Edit product form (mandatory fields)
a. Brand (varchar)
b. Product Name (varchar)
c. UPC12 Number (bigint)
d. Save button (saves changes made before returning to the Grocery List)
e. Cancel button (brings you back to the Grocery List without saving)
f. Prevent saving of empty Brand and Product Name fields
g. Warn if invalid characters are entered into the fields (e.g. UPC12 field should
accept only integers)
2. Editing a product
a. Pre-populate the form with the product’s info (i.e. if you entered this page by
clicking on the Edit button of Product A, the form should display the Brand,
Product Name, and UPC12 Number of Product A)
b. Saving should be done by the POST or PUT method only
c. Grocery List should also reflect the changes made to the product
BONUS REQUIREMENTS:
Page 1
1. Search form (validation)
a. Prevent attempts to search when both Brand and Product Name fields are
blank
b. Warn if invalid characters are entered into the fields
c. Do you want to allow a search by BOTH Brand and Product Name?
2. List of Groceries
a. Product Image (replace with Placeholder Image if Product Image is
unavailable)
b. Pagination with ability to change the number of products listed per page
Page 2
1. Edit product form (validation)
a. Product Image (file upload field)
2. Editing a product
a. Demonstrate the ability to upload an image file (JPG) upon saving, and
rename its filename to the product’s corresponding UPC12 Number before
putting the image file to a folder on your local drive or to a cloud storage.
b. Can your edit product module accept non-English Brand and Product Name?