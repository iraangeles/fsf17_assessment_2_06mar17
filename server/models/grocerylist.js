// Models for grocery 
module.exports = function(sequelize, Sequelize) {
    var GroceryList =  sequelize.define('groceryList', {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        upc12: {
            type: Sequelize.BIGINT(12),
            allowNull: false
        },
        brand: {
            type: Sequelize.STRING,
            allowNull: false
        },        
        name: {
            type: Sequelize.STRING,
            allowNull: false
        }
    }, {
        timestamps: false
        // disable the modification of table names; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        , freezeTableName: true
        , tableName: 'grocery_list'
        // , sync : {force: true}

    });
    return GroceryList;
};