var express = require("express");


var path = require("path");

var bodyParser = require("body-parser");

var Sequelize = require("sequelize");

const NODE_PORT = process.env.PORT || 8080;

const CLIENT_FOLDER = path.join(__dirname + '/../client');  
const MSG_FOLDER = path.join(CLIENT_FOLDER + '/assets/messages');


const MYSQL_USERNAME = 'root';
const MYSQL_PASSWORD = 'abcd1234';

const API_SEARCH_ENDPOINT = '/api/grocerylist';
const API_PRODUCTID_ENDPOINT = '/api/productid';

var app = express(); 

var sequelize = new Sequelize(
'grocery',
MYSQL_USERNAME,
MYSQL_PASSWORD,
{
    host: 'localhost',         // default port    : 3306
    logging: console.log,
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    }
}
);

// Loads model for department table
var GroceryList = require('./models/grocerylist')(sequelize, Sequelize);

// sequelize
//   .sync({ force: true })
//   .then(function(err) {
//     console.log('It worked!');
//   }, function (err) { 
//     console.log('An error occurred while creating the table:', err);
//   });


app.use(express.static(CLIENT_FOLDER));
app.use(bodyParser.json());



app.get(API_SEARCH_ENDPOINT + '/:brand/:name', function(req, res) {
    console.log("Searching for brand %s.....", JSON.stringify(req.params.brand));
    console.log("Searching for name %s.....", JSON.stringify(req.params.name));

    // res.status(200).end();

     GroceryList
        .findAll({
            where: { 
                $or: [
                    { brand: {$like: "%" + req.params.brand + "%"}},
                    { name: {$like: "%" + req.params.name + "%"}}
                ]
            }               
        }).then(function(gmslist){
            res
                .status(200)
                .json(gmslist);
        }).catch(function(err){
            res
                .status(500)
                .json(err);
        })  
});

app.get( API_PRODUCTID_ENDPOINT + '/:id', function(req,res){
    console.log ("Searching for id %s",req.params.id );

    GroceryList
        .find({
            where: { id: parseInt(req.params.id)  }
        }).then(function(gmslist){
            res
                .status(200)
                .json(gmslist);
        }).catch(function(err){
            res
                .status(500)
                .json(err);
        })  

});

app.put( API_PRODUCTID_ENDPOINT + '/:id', function(req,res){
    console.log ("Update for id %s", JSON.stringify(req.body)  );

    // res.status(200).json();

    GroceryList
        .update( { 
            brand: req.body.brand, 
            name: req.body.name,
            upc12: req.body.upc12
        }, {
            where: {id: req.params.id}
        }).then(function(result){
            res
                .status(200)
                .json(result);
        }).catch(function(err){
            console.log(err);
            res
                .status(500)
                .json(err);
        })


});



app.use('/bower_components', express.static( path.join(__dirname,'/../bower_components')));

app.use(express.static(path.join(__dirname,"/../client")));


app.use(function (req, res) {
    res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
});

app.use(function (err, req, res, next) {
    res.status(501).sendFile(path.join(MSG_FOLDER + '/501.html'));
});


app.listen(NODE_PORT, function () {
    console.log("Application server running at port " + NODE_PORT);
});

