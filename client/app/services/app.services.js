(function(){
    angular
        .module("MyApp")
        .service("MyService",MyService);

    MyService.$inject = ['$http'];

    function MyService($http){
        var service = this;

        console.log("service");

        // function page1Svc()

        service.searchbrand = searchbrand;
        service.searchbyid = searchbyid;
        service.updatebyid = updatebyid;

        function searchbrand(brand, name){
            return $http({
                method: 'GET',
                url: "/api/grocerylist/" + brand + "/" + name
            });
        }
    

        function searchbyid(id){
            return $http({
                method: 'GET',
                url: "/api/productid/" + id
            });

        }

        function updatebyid(product){
            return $http({
                method: 'PUT',
                url: "/api/productid/" + product.id ,
                data: product
            });

        }


    }



})();

