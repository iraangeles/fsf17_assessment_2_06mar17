(function(){

    angular
        .module("MyApp")
        .controller("Page2Ctrl",Page2Ctrl);

    Page2Ctrl.$inject = ['$window','$state', '$stateParams'  ,'MyService'];

    function Page2Ctrl ($window,$state,$stateParams,MyService){
        var vm = this;

        console.log("Page 2 Controller " + $stateParams.productId);

        vm.header = "Grocery Management System";
        vm.subheader = "Edit";

        vm.product = {};



        vm.cancel = cancel;
        vm.search = search;
        vm.save = save;

        if ($stateParams){
            search();

        }



        function cancel(){

            $state.go('page1');

        };


        function search(){

            MyService
                .searchbyid($stateParams.productId)
                .then(function(result){
                    console.log (JSON.stringify(result));
                    vm.product = result.data;
                })
                .catch(function(err){
                    console.log("Error" + err);

                })
        };

        function save(){
            console.log("Save product updates " + JSON.stringify(vm.product));

            MyService
                .updatebyid(vm.product)
                .then(function(){
                    console.log ("data updated in mysql");
                })
                .catch(function(err){
                    console.log("Error" + JSON.stringify(err) ) ;

                })

            $state.go('page1');

        };




    }



})();