(function(){

    angular
        .module("MyApp")
        .controller("Page1Ctrl",Page1Ctrl);

    Page1Ctrl.$inject = ['$window','$state','MyService'];

    function Page1Ctrl ($window,$state ,MyService){
        var vm = this;

        console.log("Page 1 Controller");

        vm.header = "Grocery Management System";
        vm.subheader = "Search";
        vm.searchBrand = "";
        vm.searchName = "";
        vm.searchResults = [];

        vm.searchgms = searchgms;
        vm.editItem = editItem;

        function searchgms(){
            console.log ("search");

            MyService
                .searchbrand(vm.searchBrand, vm.searchName )
                .then(function(result){
                    console.log (JSON.stringify(result));
                    vm.searchResults = result.data;
                })
                .catch(function(err){
                    console.log("Error" + err);

                })

        };


        function editItem(product){
            console.log("edit " + product.id);
            $state.go("page2");
        };



    }



})();